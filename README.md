# README #

A Spring Boot application showcasing the use of redis, liquibae, log4j2 logger, spring security etc.

### How do I get set up? ###

* clone the repository
* Open with any IDEA
* Configure MySQL database -> 1) Create database 2) Add privileges
* Set database properties in application.properties file.
