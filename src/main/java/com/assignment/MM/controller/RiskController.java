package com.assignment.MM.controller;

import com.assignment.MM.model.Risk;
import com.assignment.MM.repo.RiskRepository;
import com.assignment.MM.service.RiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class RiskController {

    @Autowired
    RiskService riskService;


    //ADD RISK FORM//
    @GetMapping("/risk")
    public String showAddRiskForm(Model model) {
        return riskService.showRiskForm(model);
    }

    //RISK ADDED//
    @PostMapping("/risk")
    public String addRiskForm(Risk risk) {
        riskService.addRisk(risk);
        return "redirect:/userRisks";
    }

    //USER RISKS//
    @GetMapping("/userRisks")
    public String showAllUserRisks(Model model) {
        List<Risk> riskList = riskService.showUserRisks();
        model.addAttribute("riskList", riskList);
        return "userRisks";
    }

    //EDIT USER RISKS//
    @GetMapping("/editRisks/{riskId}")
    public String editUserRisk(@PathVariable("riskId") Long riskId, HttpSession session, Model model) {
        riskService.editRisk(riskId, session, model);
        return "editRisk";
    }

    //SAVE UPDATED RISK//
    @PostMapping("/editRisks/saveRisk")
    public String saveUpdatedRisk(Risk risk, HttpSession session){
        Long riskId = Long.parseLong(session.getAttribute("riskId").toString());
        riskService.saveEditedRisk(risk, riskId);
        return "redirect:/userRisks";
    }

    //DELETE RISKS//
    @GetMapping("/deleteRisks/{riskId}")
    public String delRisk(@PathVariable("riskId") Long riskId){
        riskService.deleteRisk(riskId);
        return "redirect:/userRisks";
    }

    @GetMapping("/showRiskDetail/{riskId}")
    public String showRiskDetailForm(@PathVariable("riskId") Long riskId, Model model) {
        riskService.showRiskDetail(riskId, model);
        return "userRisks";
    }
}
