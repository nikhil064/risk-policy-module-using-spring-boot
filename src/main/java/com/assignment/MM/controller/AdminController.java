package com.assignment.MM.controller;

import com.assignment.MM.model.User;
import com.assignment.MM.service.AdminShowDetailsService;
import com.assignment.MM.service.AdminShowUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;


@Controller
public class AdminController {

    @Autowired
    AdminShowUserService adminShowUserService;

    @Autowired
    AdminShowDetailsService adminShowDetailsService;

    @GetMapping("/admin/showUsers")
    public String showUsersForm(Model model) {
        List<User> users = adminShowDetailsService.showUsersForm();
        model.addAttribute("users", users);
        return "allUsers";
    }

    @GetMapping("/admin/showUsers/{name}")
    public String showUserPolicyRiskForm(@PathVariable("name") String name, Model model) {
        adminShowUserService.showUserPolicyRisk(name, model);
        return "userPolicyRisk";
    }

    @GetMapping("/admin/showPolicyDetail/{policyId}")
    public String showPolicyDetailForm(@PathVariable("policyId") Long policyId, Model model) {
        adminShowDetailsService.showPolicyDetail(policyId, model);
        return "adminShowPolicy";
    }

    @GetMapping("/admin/showRiskDetail/{riskId}")
    public String showRiskDetailForm(@PathVariable("riskId") Long riskId, Model model) {
        adminShowDetailsService.showRiskDetail(riskId, model);
        return "adminShowRisk";
    }

    @GetMapping(value = "/admin/showLogs")
    public String showLogsForm(Model model) {
        adminShowDetailsService.showLogs(model);
        return "logs";
    }

}
