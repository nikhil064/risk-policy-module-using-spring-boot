package com.assignment.MM.controller;

import com.assignment.MM.model.*;
import com.assignment.MM.repo.*;
import com.assignment.MM.service.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;



@Controller
public class UserController {

    @Autowired
    RegisterUserService registerUserService;

    @Autowired
    LogsRepository logsRepository;

    @Autowired
    RiskPolicyService riskPolicyService;

    //REGISTER FORM//
    @GetMapping("/register")
    public String registerForm(Model model) {
        return registerUserService.showRegisterForm(model);
    }

    //REGISTER//
    @PostMapping("/register")
    public String registerFormSuccess(User user){
        return registerUserService.registerUser(user);
    }

    //LOGIN PAGE//
    @GetMapping("/login")
    public String loginPage() {
        return "login";
    }

    //HOME PAGE//
    @GetMapping("/index")
    public String showHome() {
        return "index";
    }

    //USER POLICIES AND RISK MAPPINGS//
    //caching//
    @GetMapping("/userPolicies")
    public String showPolicyRiskMappingForm(Model model) {
        riskPolicyService.showPolicyRiskMapping(model);
        return "userPolicies";
    }

    //SCHEDULER LOGS//
    @GetMapping(value = "/showLogs")
    public String showLogs(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Logs> logs = logsRepository.findByUsername(username);
        model.addAttribute("logs",logs);
        return "logs";
    }

}

