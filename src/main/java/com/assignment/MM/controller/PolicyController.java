package com.assignment.MM.controller;

import com.assignment.MM.model.Policy;
import com.assignment.MM.model.Risk;
import com.assignment.MM.repo.PolicyRepository;
import com.assignment.MM.repo.RiskRepository;
import com.assignment.MM.service.PolicyService;
import com.assignment.MM.service.RiskPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class PolicyController {

    @Autowired
    PolicyService policyService;

    @Autowired
    RiskPolicyService riskPolicyService;

    //POLICY FORM//
    @GetMapping("/policy")
    public String showPolicyForm(Model model) {
        return policyService.showPolicy(model);
    }

    //POLICY ADDED//
    @PostMapping("/policy")
    public String addPolicyForm(Policy policy, HttpSession session) {
        policyService.addPolicy(policy, session);
        return "redirect:/addRiskToPolicy";
    }

    //UPDATE POLICY//
    @GetMapping("/updatePolicy")
    public String updatePolicy(Model model){
        policyService.updatePolicyForm(model);
        return "updatePolicies";
    }

    //SHOW POLICY DETAILS//
    @GetMapping("/showPolicyDetail/{policyId}")
    public String showPolicyDetailForm(@PathVariable("policyId") Long policyId, Model model){
        policyService.showPolicyDetail(policyId, model);
        return "updatePolicies";
    }

    //EDIT POLICY FORM//
    @GetMapping("/editPolicy/{policyId}")
    public String editPolicy(@PathVariable("policyId") Long policyId, HttpSession session, Model model) {
        policyService.editPolicyPage(policyId, session, model);
        return "editPolicy";
    }

    //NEW POLICY SAVED//
    @PostMapping("/editPolicy/savePolicy")
    public String saveNewPolicy(Policy policy, HttpSession session) {
        policyService.saveEditedPolicy(policy, session);
        return "redirect:/updatePolicy";
    }

    //EDIT POLICY RISK//
    @GetMapping("/editPolicy/{policyId}/{riskId}")
    public String editPolicy(@PathVariable("policyId") Long policyId, @PathVariable("riskId") Long riskId) {
        return riskPolicyService.editPolicy(policyId, riskId);
    }

    //DELETE POLICY//
    @GetMapping(value="/deletePolicy/{policyId}")
    public String deletePolicy(@PathVariable("policyId") Long policyId) {
        policyService.deletePolicy(policyId);
        return "redirect:/updatePolicy";
    }

    //ADD RISKS TO POLICY FORM//
    @GetMapping("/addRiskToPolicy")
    public String addRiskToPolicy(Model model){
        policyService.addRiskToPolicy(model);
        return "addrisktopolicy";
    }

    //RISK ADDED TO POLICY//
    @GetMapping("/addRiskToPolicy/{riskId}")
    public String addRiskToPolicy(@PathVariable("riskId") Long riskId, HttpSession session){
        return policyService.addRiskToPolicy(riskId, session);
    }
}
