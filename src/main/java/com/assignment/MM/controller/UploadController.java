package com.assignment.MM.controller;

import com.assignment.MM.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
public class UploadController {

    @Autowired
    UploadService uploadService;

    //UPLOAD FILE FORM//
    @GetMapping("/upload")
    public String uploadPageForm() {
        return uploadService.uploadPage();
    }


    //FILE UPLOADED//
    @PostMapping("/upload")
    public String uploadFileForm(@RequestParam("file") MultipartFile[] file) throws IOException {
        uploadService.uploadFile(file);
        return "redirect:/test";
    }

    //TEST THE FILE UPLOADED//
    @GetMapping("/test")
    public String testFileForm(Model model) throws IOException {
        uploadService.testFile(model);
        return "testResults";
    }
}
