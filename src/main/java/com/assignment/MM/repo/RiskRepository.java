package com.assignment.MM.repo;

import com.assignment.MM.model.Risk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface RiskRepository extends JpaRepository<Risk, Long> {

    @Query(value = "SELECT r.* FROM risk r WHERE r.username=?1", nativeQuery = true)
    List<Risk> findRiskByUsername(String username);

    @Query(value = "SELECT r.* FROM risk r WHERE r.risk_id=?1", nativeQuery = true)
    Risk findByRiskId(Long riskId);

}
