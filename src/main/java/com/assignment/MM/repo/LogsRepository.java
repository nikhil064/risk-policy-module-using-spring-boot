package com.assignment.MM.repo;

import com.assignment.MM.model.Logs;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LogsRepository extends JpaRepository<Logs, Long> {
    List<Logs> findByUsername(String username);
}
