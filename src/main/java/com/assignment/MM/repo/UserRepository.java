package com.assignment.MM.repo;

import com.assignment.MM.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    //For register user purpose//
    User findUserByEmail(String email);

    User findUserById(Long id);

    //For Login user purpose//
    User findByUsername(String name);

    @Query(value = "SELECT u.username FROM user u WHERE u.username != 'admin'", nativeQuery = true)
    List<String> findAllUsers();

    @Query(value = "SELECT u.* FROM user u where u.username != 'admin'", nativeQuery = true)
    List<User> getUsers();


}
