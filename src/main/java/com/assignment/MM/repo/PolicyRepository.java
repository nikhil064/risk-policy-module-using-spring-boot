package com.assignment.MM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.assignment.MM.model.Policy;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PolicyRepository extends JpaRepository<Policy, Long> {

    @Query(value = "SELECT p.* FROM policy p WHERE p.username=?1", nativeQuery = true)
    List<Policy> findPolicyByUsername(String username);

    @Query(value = "SELECT p.policy_id FROM policy p WHERE p.username=?1", nativeQuery = true)
    List<Long> getPolicyFromUsername(String username);

    @Query(value = "SELECT p.* FROM policy p WHERE p.policy_id=?1", nativeQuery = true)
    Policy getPolicyFromPolicyId(Long policyId);


}
