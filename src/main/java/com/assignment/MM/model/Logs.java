package com.assignment.MM.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "logs")
public class Logs {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long log_id;
    private Long policy_id;
    private String policy_title;
    private Long risk_id;
    private String risk_title;
    private String risk_keyword;
    private String username;
    private LocalDateTime dateTime;
    private String fileName;

    public Logs() {
    }

    public Logs(Long policy_id, String policy_title, Long risk_id, String risk_title, String risk_keyword, String username, LocalDateTime dateTime, String fileName) {
        this.policy_id = policy_id;
        this.policy_title = policy_title;
        this.risk_id = risk_id;
        this.risk_title = risk_title;
        this.risk_keyword = risk_keyword;
        this.username = username;
        this.dateTime = dateTime;
        this.fileName = fileName;
    }

    public Long getLog_id() {
        return log_id;
    }

    public void setLog_id(Long log_id) {
        this.log_id = log_id;
    }

    public Long getPolicy_id() {
        return policy_id;
    }

    public void setPolicy_id(Long policy_id) {
        this.policy_id = policy_id;
    }

    public String getPolicy_title() {
        return policy_title;
    }

    public void setPolicy_title(String policy_title) {
        this.policy_title = policy_title;
    }

    public Long getRisk_id() {
        return risk_id;
    }

    public void setRisk_id(Long risk_id) {
        this.risk_id = risk_id;
    }

    public String getRisk_title() {
        return risk_title;
    }

    public void setRisk_title(String risk_title) {
        this.risk_title = risk_title;
    }

    public String getRisk_keyword() {
        return risk_keyword;
    }

    public void setRisk_keyword(String risk_keyword) {
        this.risk_keyword = risk_keyword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return "Logs{" +
                "policy_id=" + policy_id +
                ", policy_title='" + policy_title + '\'' +
                ", risk_id=" + risk_id +
                ", risk_title='" + risk_title + '\'' +
                ", risk_keyword='" + risk_keyword + '\'' +
                ", username='" + username + '\'' +
                ", dateTime=" + dateTime +
                ", fileName='" + fileName + '\'' +
                '}';
    }
}
