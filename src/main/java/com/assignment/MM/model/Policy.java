package com.assignment.MM.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "policy")
public class Policy {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long policy_id;
    private String title;
    private String description;
    private String username;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "policy_risk",
            joinColumns = @JoinColumn(name = "policy_id"),
            inverseJoinColumns = @JoinColumn(name = "risk_id"))
    private List<Risk> risks = new ArrayList<>();

    public Policy() {
    }

    public Policy(String title, String description, String username, List<Risk> risks) {
        this.title = title;
        this.description = description;
        this.username = username;
        this.risks = risks;
    }

    public Long getPolicy_id() {
        return policy_id;
    }

    public void setPolicy_id(Long policy_id) {
        this.policy_id = policy_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Risk> getRisks() {
        return risks;
    }

    public void setRisks(List<Risk> risks) {
        this.risks = risks;
    }

    public void addRisk(Risk risk){
        risks.add(risk);
        risk.getPolicies().add(this);
    }

    public void removeRisk(Risk risk){
        risks.remove(risk);
    }

    @Override
    public String toString() {
        return "Policy{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
