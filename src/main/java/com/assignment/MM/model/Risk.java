package com.assignment.MM.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "risk")
public class Risk implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long risk_id;
    private String title;
    private String description;
    private String keyword;
    private String regex;
    private int matchCount;
    private String username;

    @ManyToMany(mappedBy = "risks", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Policy> policies = new ArrayList<>();

    public Risk() {
    }

    public Risk(String title, String description, String keyword, String regex, int matchCount, String username, List<Policy> policies) {
        this.title = title;
        this.description = description;
        this.keyword = keyword;
        this.regex = regex;
        this.matchCount = matchCount;
        this.username = username;
        this.policies = policies;
    }

    public Long getRisk_id() {
        return risk_id;
    }

    public void setRisk_id(Long risk_id) {
        this.risk_id = risk_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public int getMatchCount() {
        return matchCount;
    }

    public void setMatchCount(int matchCount) {
        this.matchCount = matchCount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Policy> getPolicies() {
        return policies;
    }

    @Override
    public String toString() {
        return "Risk{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", keyword='" + keyword + '\'' +
                ", regex='" + regex + '\'' +
                ", matchCount=" + matchCount +
                ", username='" + username + '\'' +
                '}';
    }
}
