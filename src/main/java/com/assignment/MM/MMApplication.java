package com.assignment.MM;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableCaching
//Remote Dictionary Server - REDIS - in-memory key-val datastore

public class MMApplication {

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("userRisks");
    }

    public static void main(String[] args) {
        SpringApplication.run(MMApplication.class, args);
    }

}
