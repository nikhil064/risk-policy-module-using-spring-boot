package com.assignment.MM.service;

import com.assignment.MM.model.Policy;
import com.assignment.MM.model.Risk;
import com.assignment.MM.repo.PolicyRepository;
import com.assignment.MM.repo.RiskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import javax.servlet.http.HttpSession;
import java.util.*;


@Service
public class RiskService {

    @Autowired
    RiskRepository riskRepository;

    @Autowired
    PolicyRepository policyRepository;

    @Autowired
    CacheManager cm;


    private static final Logger logger = LoggerFactory.getLogger(RiskService.class);

    public String showRiskForm(Model model) {
        model.addAttribute("risk", new Risk());
        return "risk";
    }

    public void addRisk(Risk risk) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        risk.setUsername(username);
        try{
            riskRepository.save(risk);
            logger.info("Risk Added by " + username);
        }catch (Exception e){
            logger.error("Not Able to add Risk by " + username);
        }

        Objects.requireNonNull(cm.getCache("userRisks")).clear();
    }

//    @Cacheable(cacheNames = "userRisks")
    public List<Risk> showUserRisks() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Risk> riskList = new ArrayList<>();

        try{
            if(Objects.requireNonNull(cm.getCache("userRisks")).getNativeCache().toString().length() != 2){
                Map<Long, Risk> mp = (Map<Long, Risk>) cm.getCache("userRisks").getNativeCache();
                for(Map.Entry<Long,Risk> entry : mp.entrySet()){
                    Long rid = entry.getKey();
                    Risk robj = new Risk();
                    robj.setRisk_id(rid);
                    robj.setTitle(entry.getValue().getTitle());
                    robj.setUsername(entry.getValue().getUsername());
                    robj.setRegex(entry.getValue().getRegex());
                    robj.setKeyword(entry.getValue().getKeyword());
                    robj.setDescription(entry.getValue().getDescription());
                    robj.setMatchCount(entry.getValue().getMatchCount());
                    riskList.add(robj);
                }
            }
            else {
                riskList = riskRepository.findRiskByUsername(username);
                logger.info("Risks retrieved for " + username + " from database");
            }
        }
        catch (Exception e){
            logger.error("Unable to fetch Risks " + username);
            System.out.println(e.getMessage());
            System.out.println(Arrays.toString(e.getStackTrace()));
        }

        for(Risk r: riskList){
            cm.getCache("userRisks").put(r.getRisk_id(), r);
        }

        return riskList;
    }

    public void editRisk(@PathVariable("riskId") Long riskId, HttpSession session, Model model) {
        Risk risk = riskRepository.findByRiskId(riskId);
        session.setAttribute("riskId", riskId);
        model.addAttribute("riskObj", risk);
    }

    @CachePut(cacheNames = "userRisks", key = "#riskId")
    public Risk saveEditedRisk(Risk risk, Long riskId){
        Risk newRisk = riskRepository.findByRiskId(riskId);
        newRisk.setDescription(risk.getDescription());
        newRisk.setKeyword(risk.getKeyword());
        newRisk.setRegex(risk.getRegex());
        newRisk.setMatchCount(risk.getMatchCount());
        newRisk.setTitle(risk.getTitle());

        try{
            riskRepository.save(newRisk);
            logger.info("Risk - " + risk.getTitle() + " updated by " + newRisk.getUsername());
        }
        catch (Exception e){
            logger.error("Failed to update Risk " + risk.getTitle());
        }

        return newRisk;
//        cm.getCache("userRisks").put(riskId, newRisk);

    }

    @CacheEvict(cacheNames = "userRisks", key = "#riskId")
    public void deleteRisk(Long riskId){
        Risk robj = riskRepository.findByRiskId(riskId);
        List<Policy> policies = robj.getPolicies();

        for(Policy p: policies){
            p.removeRisk(robj);
            policyRepository.save(p);
        }
        riskRepository.deleteById(riskId);

//        cm.getCache("userRisks").evict(riskId);
        logger.info("Risk -" + robj.getTitle() + " Deleted by " + robj.getUsername());
    }

    public void showRiskDetail(@PathVariable("riskId") Long riskId, Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Risk> riskList = riskRepository.findRiskByUsername(username);
        model.addAttribute("riskList", riskList);
    }
}
