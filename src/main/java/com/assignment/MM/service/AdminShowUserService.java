package com.assignment.MM.service;

import com.assignment.MM.model.Logs;
import com.assignment.MM.model.Policy;
import com.assignment.MM.model.Risk;
import com.assignment.MM.repo.PolicyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdminShowUserService {

    @Autowired
    PolicyRepository policyRepository;

    public void showUserPolicyRisk(@PathVariable("name") String name, Model model) {
        Map<Policy, List<Risk>> map = new HashMap<>();
        List<Policy> userPolicies = policyRepository.findPolicyByUsername(name);

        for (Policy p : userPolicies) {
            List<Risk> risks = p.getRisks();
            map.put(p, risks);
        }

        List<Logs> data = new ArrayList<>();
        for (Map.Entry<Policy, List<Risk>> entry : map.entrySet()) {
            Policy p = entry.getKey();
            List<Risk> risks = entry.getValue();
            for (Risk riskList : risks) {

                Logs logs = new Logs();
                logs.setPolicy_id(p.getPolicy_id());
                logs.setPolicy_title(p.getTitle());
                logs.setUsername(name);
                logs.setRisk_id(riskList.getRisk_id());
                logs.setRisk_title(riskList.getTitle());
                logs.setRisk_keyword(riskList.getKeyword());
                logs.setDateTime(LocalDateTime.now());
                logs.setFileName("temp");
                data.add(logs);
            }
        }

        model.addAttribute("data", data);

    }
}
