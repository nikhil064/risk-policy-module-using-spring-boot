package com.assignment.MM.service;

import com.assignment.MM.model.User;
import com.assignment.MM.repo.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.io.File;

@Service
public class RegisterUserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    private static final Logger logger = LoggerFactory.getLogger(RegisterUserService.class);

    public String showRegisterForm(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    public String registerUser(User user) {
        User us = userRepository.findUserByEmail(user.getEmail());

        if (us != null) {
            logger.error("User Already Exists");
            return "redirect:/register?failure";
        } else {
            String new_pass = passwordEncoder.encode(user.getPassword());
            user.setPassword(new_pass);
            user.setRole("USER");
            userRepository.save(user);
            logger.info("New user registered " + user.getUsername());

            //UPLOAD DIR//
            String uploadDirectory = System.getProperty("user.dir") + "/uploads";
            File file = new File(uploadDirectory);
            if (!file.exists()) {
                file.mkdir();
            }

            //UPLOAD --> USER DIR//
            String name = user.getUsername();
            String uploadUserDirectory = System.getProperty("user.dir") + "/uploads" + "/" + name;
            File file2 = new File(uploadUserDirectory);
            if (!file2.exists()) {
                file2.mkdir();
            }
            return "redirect:/register?success";
        }

    }
}