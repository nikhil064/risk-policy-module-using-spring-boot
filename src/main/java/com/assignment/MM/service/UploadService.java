package com.assignment.MM.service;

import com.assignment.MM.model.Logs;
import com.assignment.MM.model.Policy;
import com.assignment.MM.model.Risk;
import com.assignment.MM.repo.LogsRepository;
import com.assignment.MM.repo.PolicyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UploadService {

    @Autowired
    PolicyRepository policyRepository;

    @Autowired
    LogsRepository logsRepository;

    public String uploadPage() {
        return "upload";
    }

    public void uploadFile(@RequestParam("file") MultipartFile[] file) throws IOException {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        for (MultipartFile f : file) {
            Path fileNameAndPath = Paths.get(System.getProperty("user.dir") + "/uploads" +
                    "/" + username, f.getOriginalFilename());

            try {
                Files.write(fileNameAndPath, f.getBytes());
            } catch (IOException e) {
                System.out.println("\n\n\nNo File Uploaded\n\n\n");
            }
        }
    }


    public void testFile(Model model) throws IOException {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        Map<Policy, List<Risk>> policyRisks = new HashMap<>();
        List<Policy> policies = policyRepository.findPolicyByUsername( username);

        for(Policy p: policies) {
            List<Risk> risks = p.getRisks();
            policyRisks.put(p, risks);
        }

        File source = new File(System.getProperty("user.dir") + "/uploads" + "/" + username);

        String testResults = "";
        List<Logs> logList = new ArrayList<>();
        for (File file : source.listFiles()) {
            String fileName = file.getName();

            FileInputStream input = new FileInputStream(System.getProperty("user.dir") + "/uploads"
                    + "/" + username + "/" + fileName);

            int i = 0;
            String temp = "";
            while ((i = input.read()) != -1) {
                temp+=(char)i;
            }
            input.close();


            for(Map.Entry<Policy, List<Risk>> entry : policyRisks.entrySet()) {
                Policy p = entry.getKey();
                List<Risk> risks= entry.getValue();
                for(Risk riskList: risks) {
                    String keyword = riskList.getKeyword();
                    int matchCount = riskList.getMatchCount();
                    String title = riskList.getTitle();
                    int keyCount = StringUtils.countOccurrencesOf(temp, keyword);

                    if (keyCount >= matchCount) {
                        Logs logs= new Logs();
                        logs.setPolicy_id(p.getPolicy_id());
                        logs.setPolicy_title(p.getTitle());
                        logs.setUsername(username);
                        logs.setRisk_id(riskList.getRisk_id());
                        logs.setRisk_title(title);
                        logs.setRisk_keyword(keyword);
                        logs.setDateTime(LocalDateTime.now());
                        logs.setFileName(fileName);

                        System.out.println("Risk violated by "+username+ "-->   Risk Title = " + title + ", Risk Keyword = " + keyword + " "
                                + ",Policy Id = " + p +" "+"risk id = " + riskList.getRisk_id());

                        logsRepository.save(logs);
                        logList.add(logs);
                    }
                }
            }
        }

        model.addAttribute("logList", logList);
    }
}
