package com.assignment.MM.service;

import com.assignment.MM.model.Policy;
import com.assignment.MM.model.Risk;
import com.assignment.MM.repo.PolicyRepository;
import com.assignment.MM.repo.RiskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class PolicyService {
    @Autowired
    RiskRepository riskRepository;

    @Autowired
    PolicyRepository policyRepository;

    public String showPolicy(Model model) {
        model.addAttribute("policy", new Policy());
        return "policy";
    }

    public void addPolicy(Policy policy, HttpSession session) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        policy.setUsername(username);
        policyRepository.save(policy);
        session.setAttribute("policyId", policy.getPolicy_id());
    }

    public void updatePolicyForm(Model model){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Policy> policies = policyRepository.findPolicyByUsername(username);
        model.addAttribute("policies", policies);
    }

    public void showPolicyDetail(@PathVariable("policyId") Long policyId, Model model){
        Policy policy = policyRepository.getPolicyFromPolicyId(policyId);
        model.addAttribute("policies", policy);
    }

    public void editPolicyPage(@PathVariable("policyId") Long policyId, HttpSession session, Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Risk> risks = riskRepository.findRiskByUsername(username);
        session.setAttribute("policyId", policyId);
        Policy policy = policyRepository.getPolicyFromPolicyId(policyId);
        model.addAttribute("policy",policy);
        model.addAttribute("risks",risks);
    }

    //caching
    public void addRiskToPolicy(Model model){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Risk> riskList = riskRepository.findRiskByUsername(username);
        model.addAttribute("riskList", riskList);
    }

    public String addRiskToPolicy(@PathVariable("riskId") Long riskId, HttpSession session){
        Long policyId = Long.parseLong(session.getAttribute("policyId").toString());
        Risk r = riskRepository.findByRiskId(riskId);
        Policy p = policyRepository.getPolicyFromPolicyId(policyId);

        List<Risk> existingRisks = p.getRisks();
        boolean flag = false;
        for (Risk risk : existingRisks) {
            if (risk.getRisk_id().equals(riskId)) {
                flag = true;
                break;
            }
        }

        if (flag) {
            return "redirect:/addRiskToPolicy?failure";
        } else {
            p.addRisk(r);
            policyRepository.save(p);
            return "redirect:/addRiskToPolicy?success";
        }
    }

    public void saveEditedPolicy(Policy policy, HttpSession session) {
        Long policyId = Long.parseLong(session.getAttribute("policyId").toString());
        Policy newPolicy;
        newPolicy = policyRepository.getPolicyFromPolicyId(policyId);
        newPolicy.setDescription(policy.getDescription());
        newPolicy.setTitle(policy.getTitle());
        policyRepository.save(newPolicy);
    }

    public void deletePolicy(@PathVariable("policyId") Long policyId) {
        policyRepository.deleteById(policyId);
    }
}
