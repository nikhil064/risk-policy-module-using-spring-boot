package com.assignment.MM.service;

import com.assignment.MM.model.Logs;
import com.assignment.MM.model.Policy;
import com.assignment.MM.model.Risk;
import com.assignment.MM.model.User;
import com.assignment.MM.repo.LogsRepository;
import com.assignment.MM.repo.PolicyRepository;
import com.assignment.MM.repo.RiskRepository;
import com.assignment.MM.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class AdminShowDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PolicyRepository policyRepository;

    @Autowired
    RiskRepository riskRepository;

    @Autowired
    LogsRepository logsRepository;

    public List<User> showUsersForm() {
        return userRepository.getUsers();
    }

    public void showPolicyDetail(@PathVariable("policyId") Long policyId, Model model) {
        Policy policy = policyRepository.getPolicyFromPolicyId(policyId);
        model.addAttribute("policy", policy);
    }

    public void showRiskDetail(@PathVariable("riskId") Long riskId, Model model) {
        Risk risk = riskRepository.findByRiskId(riskId);
        model.addAttribute("risk", risk);
    }

    public void showLogs(Model model) {
        List<Logs> logs = logsRepository.findAll();
        model.addAttribute("logs",logs);
    }
}
