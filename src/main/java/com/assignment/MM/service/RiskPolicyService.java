package com.assignment.MM.service;

import com.assignment.MM.model.Logs;
import com.assignment.MM.model.Policy;
import com.assignment.MM.model.Risk;
import com.assignment.MM.repo.PolicyRepository;
import com.assignment.MM.repo.RiskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RiskPolicyService {

    @Autowired
    PolicyRepository policyRepository;

    @Autowired
    RiskRepository riskRepository;


    public void showPolicyRiskMapping(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Policy> policy = policyRepository.findPolicyByUsername(username);

        Map<Policy,List<Risk> > map = new HashMap<>();
        for(Policy userp: policy) {
            List<Risk> risks = userp.getRisks();
            map.put(userp,risks);
        }

        List<Logs> logs = new ArrayList<>();
        for(Map.Entry<Policy,List<Risk>> entry : map.entrySet()) {
            Policy po = entry.getKey();
            List<Risk> riskList = entry.getValue();

            for(Risk r: riskList) {
                Logs logsvariable = new Logs();
                logsvariable.setPolicy_id(po.getPolicy_id());
                logsvariable.setRisk_id(r.getRisk_id());
                logsvariable.setRisk_title(r.getTitle());
                logsvariable.setPolicy_title(po.getTitle());
                logsvariable.setRisk_keyword(r.getKeyword());
                logsvariable.setUsername(SecurityContextHolder.getContext().getAuthentication().getName());
                logsvariable.setDateTime(LocalDateTime.now());
                logs.add(logsvariable);
            }
        }
        model.addAttribute("logs",logs);
    }

    public String editPolicy(@PathVariable("policyId") Long policyId, @PathVariable("riskId") Long riskId) {
        Risk r = riskRepository.findByRiskId(riskId);
        Policy p = policyRepository.getPolicyFromPolicyId(policyId);

        List<Risk> existingRisks = p.getRisks();
        boolean flag = false;
        for (Risk risk : existingRisks) {
            if (risk.getRisk_id() == r.getRisk_id()) {
                flag = true;
                break;
            }
        }

        if (flag) {
            return "redirect:/editPolicy/{policyId}?failure";
        } else {
            p.addRisk(r);
            policyRepository.save(p);
            return "redirect:/editPolicy/{policyId}?success";
        }
    }
}
