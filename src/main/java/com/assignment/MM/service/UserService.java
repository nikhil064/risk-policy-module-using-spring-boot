package com.assignment.MM.service;

import com.assignment.MM.model.User;
import com.assignment.MM.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {

        //Fetch by User Email to avoid multiple returns
        User user = userRepository.findByUsername(name);

        if(user == null) {throw new UsernameNotFoundException("Invalid Username or Password");}
        return new UserDetailsImpl(user);
    }
}
