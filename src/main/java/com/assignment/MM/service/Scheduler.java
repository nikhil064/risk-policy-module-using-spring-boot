package com.assignment.MM.service;
import com.assignment.MM.model.Logs;
import com.assignment.MM.model.Policy;
import com.assignment.MM.model.Risk;
import com.assignment.MM.repo.*;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Transactional
@Component
public class Scheduler {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PolicyRepository policyRepository;

    @Autowired
    LogsRepository logsRepository;


    @Scheduled(initialDelay = 200000L,fixedRate = 500000L)
    public void scheduler() throws IOException {
        List<String> users = userRepository.findAllUsers();
        for(String username:users) {

            Map<Policy,List<Risk>> policyRisks = new HashMap<>();
            List<Policy> userPolicies = policyRepository.findPolicyByUsername(username);

            for(Policy p: userPolicies) {
                List<Risk> risks = p.getRisks();
                policyRisks.put(p,risks);
            }

            File sourceFolder = new File(System.getProperty("user.dir") + "/uploads" + "/" + username);
            for(File sourceFile: Objects.requireNonNull(sourceFolder.listFiles())) {
                String filename = sourceFile.getName();
                FileInputStream fin = new FileInputStream(System.getProperty("user.dir")+"/uploads"+"/"+username+"/"+filename);
                String temp="";
                int i;
                while((i=fin.read())!=-1){
                    temp+=(char)i;
                }
                fin.close();
                for(Map.Entry<Policy,List<Risk>> entry : policyRisks.entrySet()) {
                    Policy p = entry.getKey();
                    List<Risk> risks= entry.getValue();
                    for(Risk riskList: risks) {
                        String keyword = riskList.getKeyword();
                        int matchCount = riskList.getMatchCount();
                        String title = riskList.getTitle();
                        int keyCount = StringUtils.countOccurrencesOf(temp, keyword);

                        if (keyCount >= matchCount) {
                            Logs logs= new Logs();
                            logs.setPolicy_id(p.getPolicy_id());
                            logs.setPolicy_title(p.getTitle());
                            logs.setUsername(username);
                            logs.setRisk_id(riskList.getRisk_id());
                            logs.setRisk_title(title);
                            logs.setRisk_keyword(keyword);
                            logs.setDateTime(LocalDateTime.now());
                            logs.setFileName(filename);

                            System.out.println("Risk violated by "+username+ "-->   Risk Title = " + title + ", Risk Keyword = " + keyword + " "
                                    + ",Policy Id = " + p +" "+"risk id = " + riskList.getRisk_id());

                            logsRepository.save(logs);
                        }
                    }
                }
            }
        }
    }
}
