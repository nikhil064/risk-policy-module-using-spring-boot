package com.assignment.MM.controller;

import com.assignment.MM.model.User;
import com.assignment.MM.repo.UserRepository;
import com.assignment.MM.service.AdminShowDetailsService;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.ui.Model;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class AdminControllerTest {

    @Mock
    AdminShowDetailsService adminShowDetailsService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void showUsersFormTest() throws Exception {

        String email = "email.com";
        User us = new User("nik", email, "pass", "role");

        when(adminShowDetailsService.showUsersForm()).thenReturn(Arrays.asList(us));

        mockMvc.perform(MockMvcRequestBuilders.get("/admin/showUsers"))
                .andExpect(status().isOk())
                .andExpect(view().name("allUsers"));

        verify(adminShowDetailsService, times(1)).showUsersForm();
    }
}